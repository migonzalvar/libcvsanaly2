#!/usrbin/env python

import sys
from storm.locals import *
from libcvsanaly2.Database import *
from libcvsanaly2.Graphs.PychaGraph import PychaGraph

ctx = DBOptionContext ()
ctx.parse_options (sys.argv[1:])
db = ctx.create_database ()
store = Store (db)

query = "select ft.type, count(a.id) from actions a, file_types ft " + \
        "where a.file_id = ft.file_id group by ft.type"
result = store.execute (query)
data = [(type, count) for type, count in result]

gfx = PychaGraph (PychaGraph.PIE)
dataset = [(rs[0], [[0, rs[1]]]) for rs in data]
options = {
    'axis': {
        'x': {
            'ticks': [dict(v=i, label=d[0]) for i, d in enumerate (data)]
        }
    },
    'colorScheme': 'blue',
    'background': {
        'baseColor': '#ffffff'
    },
    'padding': {
        'left': 100
    },
    'legend': {
        'position': {
            'top': 10,
            'left': 0
        }
    },
    'title' : 'Types of commits in LibreSoft Tools'
}
gfx.render ("commits-types.png", 500, 400, dataset, options)

query = "select p.name, ft.type, count(a.id) from scmlog s, people p, actions a, file_types ft " + \
        "where s.id = a.commit_id and s.committer = p.id and a.file_id = ft.file_id " + \
        "group by ft.type,p.name order by 1"
result = store.execute (query)
data = {}
for committer, type, count in result:
    data.setdefault (committer, []).append ((type, count))

for committer in data:
    dataset = [(rs[0], [[0, rs[1]]]) for rs in data[committer]]
    options['title'] = 'Types of commits in LibreSoft Tools for %s' % (committer)
    options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data[committer])]
    gfx.render ("%s.png" % (committer), 500, 400, dataset, options)

store.close ()
