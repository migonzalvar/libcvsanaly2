import sys
import datetime
import re

from storm.locals import *
from libcvsanaly2.Database import *
from libcvsanaly2.Graphs.PychaGraph import PychaGraph

ctx = DBOptionContext ()
ctx.parse_options (sys.argv[1:])
db = ctx.create_database ()
store = Store (db)

# General options for graphs
global_options = {
    'colorScheme': '#4a6dbc',
    'background': {
        'chartColor': '#ffeeff',
        'baseColor': '#ffffff',
        'lineColor': '#444444'
    },
    'axis': {
        'x': {},
        'y': {
            'ticksCount': 10
        }
    },
    'legend': {
        'hide': True
    },
    'padding': {
        'top': 20,
        'left': 40,
        'right': 20,
        'bottom': 40
    }
}

query = "select r.name, count(s.id) from scmlog s, repositories r where s.repository_id = r.id group by s.repository_id order by 2 desc"
result = store.execute (query)
data = [item for item in result]
dataset = (("committers", [(i, l[1]) for i, l in enumerate (data)]),)
options = global_options.copy ()
options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data)]
options['axis']['y']['rotate'] = 25
options['padding']['left'] = 60
options['title'] = "Top commits per tool"

gfx = PychaGraph (PychaGraph.HORIZONTAL_BAR)
gfx.render ("ltools-top-commits.png", 500, 400, dataset, options)

query = "select date_format(s.date, \"%m-%Y\") df, r.name, count(s.id) from scmlog s, repositories r where s.repository_id = r.id group by s.repository_id,df order by s.date"
result = store.execute (query)
modules_data = {}
data = []
for label, module, n_commits in result:
    print label
    modules_data.setdefault (module, []).append ((label, n_commits))
    if label not in data:
        data.append (label)
    
dataset = []
for module in modules_data:
    dataset.append ((module, [(i, l[1]) for i, l in enumerate (modules_data[module])]))
    

options = global_options.copy ()
options['legend']['hide'] = False
options['padding']['left'] = 180
options['axis']['x']['ticks'] = [dict(v=i, label=l if i % 3 == 0 or data[i] == data[-1] else '') for i, l in enumerate (data)]
options['axis']['x']['label'] = 'Time (months)'
options['title'] = "Libresoft Tools commits activity"

gfx = PychaGraph (PychaGraph.LINE)
gfx.render ("ltools-activity-modules.png", 600, 400, dataset, options)

store.close ()
