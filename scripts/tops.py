#!/usr/bin/env python

import sys
from storm.locals import *
from libcvsanaly2.Database import *
from libcvsanaly2.Graphs.PychaGraph import PychaGraph

ctx = DBOptionContext ()
ctx.parse_options (sys.argv[1:])
db = ctx.create_database ()
store = Store (db)

gfx = PychaGraph (PychaGraph.HORIZONTAL_BAR)
options = {
    'colorScheme': 'red',
    'background': {
        'chartColor': '#ffeeff',
        'baseColor': '#ffffff',
        'lineColor': '#444444'
    },
    'axis': {
        'x': {},
        'y': {
            'tickPrecision': 0
        }
    },
    'legend': {
        'hide': True
    },
    'padding': {
        'top': 20,
        'left': 70,
        'right': 20,
        'bottom': 20
    }
}    
        
# Top committers
query = "select p.name, count(s.id) from scmlog s, people p " + \
        "where p.id = s.committer group by p.name order by 2 desc"
result = store.execute (query)
data = [item for item in result]
dataset = (("committers", [(i, l[1]) for i, l in enumerate (data)]),)
options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data)]
options['axis']['y']['ticksCount'] = len (dataset[0][1])
options['title'] = "Top committers"
gfx.render ("top-committers.png", 500, 400, dataset, options)

# Top coders
# TODO
