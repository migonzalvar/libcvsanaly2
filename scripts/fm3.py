#!/usr/bin/env python

import os
from storm.locals import *
from libcvsanaly2.Database import *
from libcvsanaly2.Graphs.PychaGraph import PychaGraph
from libcvsanaly2.Exporter import Exporter

class Report:

    # General options for graphs
    options = {
        'colorScheme': '#4a6dbc',
        'background': {
            'chartColor': '#ffeeff',
            'baseColor': '#ffffff',
            'lineColor': '#444444'
        },
        'axis': {
            'x': {},
            'y': {
                'tickPrecision': 0
            }
        },
        'legend': {
            'hide': True
        },
        'padding': {
            'top': 20,
            'left': 40,
            'right': 20,
            'bottom': 40
        }
    }
    
    def __init__ (self, driver, in_db, out_db):
        self.driver = driver
        self.db = out_db

        out_store = Store (self.db)
        self.__create_tables (out_store)

        # Fill tables
        in_store = Store (in_db)
        self.__fill_commits_in_time (in_store, out_store)
        self.__fill_committers_in_time (in_store, out_store)
        self.__fill_new_committers_in_time (in_store, out_store)
        self.__fill_commits_types (in_store, out_store)

        out_store.close ()
        in_store.close ()

    def __create_tables (self, store):
        if self.driver == 'sqlite':
            store.execute ('create table commits (' + 
                           'id integer primary key autoincrement, ' + 
                           'month varchar, ' + 
                           'n_commits integer, ' +
                           'sum_commits integer ' +
                           ')', noresult=True)
            store.execute ('create table committers (' +
                           'id integer primary key autoincrement, ' +
                           'year varchar, ' +
                           'n_committers integer' +
                           ')', noresult=True)
            store.execute ('create table new_committers (' +
                           'id integer primary key autoincrement, ' +
                           'year varchar, ' +
                           'n_committers integer, ' +
                           'sum_committers integer' +
                           ')', noresult=True)
            store.execute ('create table commits_types (' +
                           'id integer primary key autoincrement, ' +
                           'type varchar, ' +
                           'n_commits integer' +
                           ')', noresult=True)
        elif self.driver == 'mysql':
            store.execute ('create table commits (' +
                           'id integer auto_increment, ' +
                           'month varchar(255), ' +
                           'n_commits integer, ' +
                           'sum_commits integer, ' +
                           'PRIMARY KEY (id)' +
                           ')', noresult=True)
            store.execute ('create table committers (' +
                           'id integer auto_increment, ' +
                           'year varchar(10), ' +
                           'n_committers integer, ' +
                           'PRIMARY KEY (id)' +
                           ')', noresult=True)
            store.execute ('create table new_committers (' +
                           'id integer auto_increment, ' +
                           'year varchar(10), ' +
                           'n_committers integer, ' +
                           'sum_committers integer, ' +
                           'PRIMARY KEY (id)' +
                           ')', noresult=True)
            store.execute ('create table commits_types (' +
                           'id integer auto_increment, ' +
                           'type varchar(255), ' +
                           'n_commits integer, ' +
                           'PRIMARY KEY (id)' +
                           ')', noresult=True)
        else:
            print "Unsupported databse %s" % (self.driver)

        store.commit ()

    def __fill_commits_in_time (self, in_store, out_store):
        query = "select " + format_date (driver, "%m-%Y", "date") + " month, count(id) " + \
                "from scmlog group by month order by date"
        result = in_store.execute (query)
        data = [(month, n_commits) for (month, n_commits) in result]
        for i, (month, n_commits) in enumerate (data):
            out_store.execute ("insert into commits " +
                               "(month, n_commits, sum_commits) " +
                               "values (?, ?, ?)",
                               (month, n_commits, sum ([n for (m,n) in data[:i + 1]])),
                               True)
        out_store.commit ()

    def __fill_committers_in_time (self, in_store, out_store):
        query = "select " + format_date (driver, "%Y", "date") + " year, count(distinct committer) " + \
                "from scmlog group by year"
        result = in_store.execute (query)
        for year, n_committers in result:
            out_store.execute ("insert into committers " +
                               "(year, n_committers) " +
                               "values (?, ?)",
                               (year, n_committers),
                               True)
        out_store.commit ()

    def __fill_new_committers_in_time (self, in_store, out_store):
        query = "select " + format_date (driver, "%Y", "t.md") + " year, count(t.committer) " + \
                "from (select committer, min(date) md from scmlog group by committer) t " + \
                "group by year"
        result = in_store.execute (query)
        data = [(year, n_committers) for (year, n_committers) in result]
        for i, (year, n_committers) in enumerate (data):
            out_store.execute ("insert into new_committers " +
                               "(year, n_committers, sum_committers) " +
                               "values (?, ?, ?)",
                               (year, n_committers, sum ([n for (y,n) in data[:i + 1]])),
                               True)
        out_store.commit ()

    def __fill_commits_types (self, in_store, out_store):
        query = "select ft.type, count(a.id) from actions a, file_types ft " + \
                "where a.file_id = ft.file_id group by ft.type"
        result = in_store.execute (query)
        for type, n_commits in result:
            out_store.execute ("insert into commits_types " +
                               "(type, n_commits) " +
                               "values (?, ?) ",
                               (type, n_commits),
                               True)
        out_store.commit ()

    def __plot_commits_in_time (self, store, outdir):
        gfx_lines = PychaGraph (PychaGraph.LINE)

        result = store.execute ("select month, n_commits, sum_commits from commits")
        data = [item for item in result]

        dataset = (("total-commits-per-month", [(i, l[1]) for i, l in enumerate (data)]),)
        
        options = self.options.copy ()
        options['axis']['x']['ticks'] = [dict(v=i, label=d[0] if i % 7 == 0 or data[i] == data[-1] else '') for i, d in enumerate (data)]
        options['axis']['y']['tickCount'] = 10
        options['title'] = "Total commits per month"

        filename = os.path.join (outdir, "total-commits-month.png")
        gfx_lines.render (filename, 400, 400, dataset, options)
        
        dataset = (("total-commits-per-month-sum", [(i, l[2]) for i, l in enumerate (data)]),)
        
        options['title'] = "Total commits per month (sum)"

        filename = os.path.join (outdir, "total-commits-month-sum.png")
        gfx_lines.render (filename, 400, 400, dataset, options)

    def __plot_committers_in_time (self, store, outdir):
        gfx_bar = PychaGraph (PychaGraph.VERTICAL_BAR)

        result = store.execute ("select year, n_committers from committers")
        data = [item for item in result]

        dataset = (("total-committers-per-year", [(i, l[1]) for i, l in enumerate (data)]),)

        options = self.options.copy ()
        options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data)]
        options['axis']['y']['tickCount'] = max ([item[1] for item in data])
        options['title'] = "Total committers per year"

        filename = os.path.join (outdir, "total-committers-year.png")
        gfx_bar.render (filename, 400, 400, dataset, options)

    def __plot_new_committers_in_time (self, store, outdir):
        gfx_bar = PychaGraph (PychaGraph.VERTICAL_BAR)

        result = store.execute ("select year, n_committers, sum_committers from new_committers")
        data = [item for item in result]

        dataset = (("new-committers-per-year", [(i, l[1]) for i, l in enumerate (data)]),)

        options = self.options.copy ()
        options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data)]
        options['axis']['y']['tickCount'] = max ([item[1] for item in data])
        options['title'] = "New committers per year"

        filename = os.path.join (outdir, "new-committers-year.png")
        gfx_bar.render (filename, 400, 400, dataset, options)
        
        dataset = (("new-committers-per-year-sum", [(i, l[2]) for i, l in enumerate (data)]),)
        
        options['title'] = "New committers per year (sum)"

        filename = os.path.join (outdir, "new-committers-year-sum.png")
        gfx_bar.render (filename, 400, 400, dataset, options)

    def __plot_commits_types (self, store, outdir):
        gfx_pie = PychaGraph (PychaGraph.PIE)

        result = store.execute ("select type, n_commits from commits_types")
        data = [item for item in result]
        
        dataset = [(l[0], [[0, l[1]]]) for i, l in enumerate (data)]

        options = self.options.copy ()
        options['axis']['x']['ticks'] = [dict(v=i, label=d[0]) for i, d in enumerate (data)]
        options['title'] = "Commits types"

        filename = os.path.join (outdir, "commits-types.png")
        gfx_pie.render (filename, 400, 400, dataset, options)
        
    def graphs (self, outputdir):
        store = Store (self.db)
        outdir = os.path.join (outputdir, "graphs")
        try:
            os.mkdir (outdir)
        except OSError, e:
            if e.errno == 17:
                pass
            else:
                raise e
        
        self.__plot_commits_in_time (store, outdir)
        self.__plot_committers_in_time (store, outdir)
        self.__plot_new_committers_in_time (store, outdir)
        self.__plot_commits_types (store, outdir)

        store.close ()

    def dumps (self, outputdir):
        pass

    def csv (self, outputdir):
        pass
        

if __name__ == '__main__':
    import sys
    
    long_opts = ["db-output-user=", "db-output-password=", "db-output-hostname=", "db-output-database=" ]

    ctx = DBOptionContext ()
    opts, args = ctx.parse_options (sys.argv[1:], None, long_opts)

    user = passwd = database = None
    hostname = 'localhost'
    outputdir = '/tmp'
    for opt, value in opts:
        if opt in ("--db-output-user",):
            user = value
        elif opt in ("--db-output-password",):
            passwd = value
        elif opt in ("--db-output-hostname",):
            hostname = value
        elif opt in ("--db-output-database",):
            database = value
        elif opt in ("--output-dir", "-o"):
            outputdir = value

    driver = ctx.get_driver ()
        
    if driver == 'sqlite':
        conn_uri =  driver + ':' + database
    elif passwd is not None:
        conn_uri = driver + '://' + user + ':' + passwd + '@' + hostname + '/' + database
    else:
        conn_uri = driver + '://' + user + '@' + hostname + '/' + database

    in_db = ctx.create_database ()
    out_db = create_database (conn_uri)

    rep = Report (driver, in_db, out_db)
    rep.graphs (outputdir)

    
