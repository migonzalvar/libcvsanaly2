#!/usr/bin/env python

import sys
import datetime
from storm.locals import *
from libcvsanaly2.Database import *
from libcvsanaly2.Graphs.PychaGraph import PychaGraph

ctx = DBOptionContext ()
ctx.parse_options (sys.argv[1:])
db = ctx.create_database ()
store = Store (db)

date_func = format_date (ctx.get_driver (), "%m-%Y", "date")
query = "select %s fd, count(*) from scmlog " % (date_func) + \
        "group by fd order by date"
result = store.execute (query)
data = [(label, n_commits) for label, n_commits in result]
store.close ()

dataset = (('activity', [(i, l[1]) for i, l in enumerate (data)]),)
options = {
    'axis': {
        'x': {
            'ticks': [dict(v=i, label=l[0] if i % 7 == 0 or data[i] == data[-1] else '') for i, l in enumerate (data)],
            'label': 'Time (months)',
            'rotation': 25
        },
        'y': {
            'tickCount': 10,
            'label': 'commits'
            }
    },
    'background': {
        'color': '#eeeeff',
        'baseColor': '#ffffff',
        'lineColor': '#444444'
    },
    'colorScheme': 'blue',
    'legend': {
        'hide': True,
    },
    'padding': {
        'top': 40,
        'left': 40,
        'bottom': 40,
    },
    'title': 'Commits activity'
    }

gfx = PychaGraph (PychaGraph.LINE)
gfx.render ("activity.png", 600, 400, dataset, options)

dataset = (('activity', [(i, l[1]) for i, l in enumerate (data)]),)
