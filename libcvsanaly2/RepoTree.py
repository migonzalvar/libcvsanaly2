# Copyright (C) 2008 LibreSoft
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors :
#       Carlos Garcia Campos <carlosgc@gsyc.escet.urjc.es>

class RepoTree:

    def __init__ (self):
        self.files = {}
        self.tops = []
        self.adj = {}

    def add_file (self, file_id, file_name):
        self.files[file_id] = file_name

    def add_link (self, parent, child):
        l = self.adj.setdefault (parent == -1 and child or parent, [])
        if parent == -1:
            self.tops.append (child)
        else:
            l.append (child)

    def get_tops (self):
        return self.tops

    def get_file_name (self, file_id):
        return self.files.get (file_id, None)

    def get_children (self, file_id):
        return self.adj.get (file_id, [])

    def is_parent (self, file_id):
        return file_id in self.adj

    def merge (self, other, to_id, from_id):
#        print "DBG: merging tree starting at %d -> %d" % (from_id, to_id)
        if not self.is_parent (to_id) and not other.is_parent (from_id):
#            print "DBG: nothing to merge"
            return

        if not self.is_parent (to_id):
            # Just copy all the children
#            print "DBG: I don't have any children, simple merge"
            for node in other.get_children (from_id):
                self.add_link (to_id, node)
            return

#        print "DBG: my children at %d" % (to_id),self.adj[to_id]
#        print "DBG: his children at %d" % (from_id),other.get_children (from_id)
        
        new_nodes = []
        for node in [n for n in self.adj[to_id]]:
            for cnode in other.get_children (from_id):
                if self.get_file_name (node) == other.get_file_name (cnode):
                    # Recursive merge dirs
                    self.merge (other, node, cnode)
                    if cnode in new_nodes:
                        new_nodes.remove (cnode)
                else:
                    new_nodes.append (cnode)

#        print "DBG: new nodes",new_nodes
        self.adj[to_id].extend (new_nodes)
                        
        
