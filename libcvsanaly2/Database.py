# Copyright (C) 2008 LibreSoft
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Authors :
#       Carlos Garcia Campos <carlosgc@gsyc.escet.urjc.es>

from storm.locals import *
from storm.exceptions import *
import datetime

# Storm object
class DBRepository (object):

    __storm_table__ = "repositories"

    id = Int (primary = True)
    uri = Unicode ()
    name = Unicode ()
    type = Unicode ()

class DBPerson (object):

    __storm_table__ = "people"

    id = Int (primary = True)
    name = Unicode ()
    email = Unicode ()
    
class DBLog (object):

    __storm_table__ = "scmlog"
    
    id = Int (primary = True)
    rev = Unicode ()
    committer_id = Int ()
    committer = Reference (committer_id, DBPerson.id)
    author_id = Int ()
    author = Reference (author_id, DBPerson.id)
    date = DateTime ()
    message = Unicode ()
    composed_rev = Bool ()
    repository_id = Int ()
    repository = Reference (repository_id, DBRepository.id)

class DBFile (object):

    __storm_table__ = "files"

    id = Int (primary = True)
    file_name = Unicode ()
    repository_id = Int ()
    repository = Reference (repository_id, DBRepository.id)

class DBFileLink (object):

    __storm_table__ = "file_links"

    id = Int (primary = True)
    parent_id = Int ()
    parent = Reference (parent_id, DBFile.id)
    file_id = Int ()
    file = Reference (file_id, DBFile.id)
    commit_id = Int ()
    commit = Reference (commit_id, DBLog.id)
    
class DBBranch (object):

    __storm_table__ = "branches"

    id = Int (primary = True)
    name = Unicode ()
    
class DBAction (object):
    
    __storm_table__ = "actions"
    
    id = Int (primary = True)
    type = Unicode ()
    file_id = Int ()
    file = Reference (file_id, DBFile.id)
    commit_id = Int ()
    commit = Reference (commit_id, DBLog.id)
    branch_id = Int ()
    branch = Reference (branch_id, DBBranch.id)

class DBFileCopy (object):

    __storm_table__ = "file_copies"

    id = Int (primary = True)
    to_id = Int ()
    to = Reference (to_id, DBFile.id)
    from_id = Int ()
    _from = Reference (from_id, DBFile.id)
    from_commit_id = Int ()
    from_commit = Reference (from_commit_id, DBLog.id)
    new_file_name = Unicode ()
    action_id = Int ()
    action = Reference (action_id, DBAction.id)

class DBTag (object):

    __storm_table__ = "tags"

    id = Int (primary = True)
    name = Unicode ()

class DBTagRev (object):

    __storm_table__ = "tag_revisions"

    id = Int (primary = True)
    tag_id = Int ()
    tag = Reference (tag_id, DBTag.id)
    commit_id = Int ()
    commit = Reference (commit_id, DBLog.id)


# Database command line options parsing
class DBOptionContext:

    short_opts = "hVu:p:d:H:"
    long_opts = ["help", "version", "db-user=", "db-password=", "db-hostname=", "db-database=", "db-driver="]
    
    def __init__ (self):
        self.driver = 'mysql'
        self.user = None
        self.password = None
        self.database = None
        self.hostname = 'localhost'
    
    def get_help_string (self):
        return """
Database:

      --db-driver    Output database driver [sqlite|mysql|postgres] (mysql)
  -u, --db-user      Database user name
  -p, --db-password  Database user password
  -d, --db-database  Database name
  -H, --db-hostname  Name of the host where database server is running (localhost)
"""

    def parse_options (self, argv, extra_short = None, extra_long = None):
        import getopt

        retval = []
        
        short_opts = self.short_opts
        if extra_short is not None:
            short_opts += extra_short
        long_opts = self.long_opts[:]
        if extra_long is not None:
            long_opts.extend (extra_long)
            
        try:
            opts, args = getopt.getopt (argv, short_opts, long_opts)
        except getopt.GetoptError, e:
            # TODO: exception
            print e
            return

        for opt, value in opts:
            if opt in ("-u", "--db-user"):
                self.user = value
            elif opt in ("-p", "--db-password"):
                self.password = value
            elif opt in ("-H", "--db-hostname"):
                self.hostname = value
            elif opt in ("-d", "--db-database"):
                self.database = value
            elif opt in ("--db-driver", ):
                self.driver = value
            else:
                retval.append ((opt, value))

        if self.database is None:
            # TODO: exception
            pass

        return retval, args

    def create_database (self):
        if self.database is None:
            # TODO: Exception not parsed
            return None
        if self.driver == 'sqlite':
            conn_uri =  self.driver + ':' + self.database
        elif self.password is not None:
            conn_uri = self.driver + '://' + self.user + ':' + self.password + '@' + self.hostname + '/' + self.database
        else:
            conn_uri = self.driver + '://' + self.user + '@' + self.hostname + '/' + self.database
    
        return create_database (conn_uri)

    def get_driver (self):
        if self.database is None:
            # TODO: Exception not parsed
            return None
        return self.driver

    def get_user (self):
        if self.database is None:
            # TODO: Exception not parsed
            return None
        return self.user

    def get_hostname (self):
        if self.database is None:
            # TODO: Exception not parsed
            return None
        return self.hostname

    def get_database (self):
        if self.database is None:
            # TODO: Exception not parsed
            return None
        return self.database
    
        # TODO: Try to connect to database 
    #try:
    #    db.get_store ().close ()
    #    return db
    #except CAAccessDenied:
    #    if password is None:
    #        import sys, getpass

            # FIXME: catch KeyboardInterrupt exception
            # FIXME: it only works on UNIX (/dev/tty),
            #  not sure whether it's bug or a feature, though
    #        oldout, oldin = sys.stdout, sys.stdin
    #        sys.stdin = sys.stdout = open ('/dev/tty', 'r+')
    #        password = getpass.getpass ()
    #        sys.stdout, sys.stdin = oldout, oldin
            
    #        return get_database (driver, database, username, password, hostname)
    #    raise CAAccessDenied



# Date functions
_format_date_funcs = {
    'sqlite' : lambda f, d: "strftime('%s', %s)" % (f, d),
    'mysql'  : lambda f, d: "date_format(%s, '%s')" % (d, f)
}

_format_convert_patts = {
    'mysql' : [("%i", "%M")]
}

def convert_format (driver, format):
    if driver not in _format_convert_patts:
        return format

    for old, new in _format_convert_patts[driver]:
        # FIXME: %%i
        return format.replace (old, new)

def format_date (driver, format, date):
    '''
    %d Day of the month (01-31)
    %m Month (01-12)
    %Y Year four digits
    %H Hour (00-23)
    %M Minutes (00-59) -> %i
    %S Seconds (00-59)
    %w Day of week (0-6 sunday==0)
    '''
    return _format_date_funcs[driver] (convert_format (driver, format), date)

