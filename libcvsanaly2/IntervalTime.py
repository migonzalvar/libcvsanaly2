import datetime
import calendar

class IntervalTime:
    
    MINUTE = datetime.timedelta (minutes=1)
    HOUR = datetime.timedelta (hours=1)
    DAY = datetime.timedelta (days=1)
    MONTH = datetime.timedelta (days=30)
    YEAR = datetime.timedelta (days=365)

    def __init__ (self, start, end = None, step = None):
        self.__dict__ = { 'start'    : start,
                          'end'      : end,
                          'step'     : step,
                          '_current' : start}

        if end is None:
            self.__dict__['end'] = datetime.datetime.now ()

        if step is None:
            self.__dict__['step'] = self.DAY

    def __getattr__ (self, name):
        return self.__dict__[name]

    def __iter__ (self):
        return self

    def next (self):
        if self._current == self.end:
            self.__dict__['_current'] = self.start
            raise StopIteration

        start = self._current
        nxt = self._current + self.step
        self.__dict__['_current'] = min (nxt, self.end)
        
        return IntervalTime (start, self._current)

if __name__ == '__main__':
    intervals = {}

    today = datetime.datetime.today ()

    intervals["This hour in minutes"] = IntervalTime (datetime.datetime (today.year, today.month, today.day, today.hour), step=IntervalTime.MINUTE)
    intervals["Today in hours"] = IntervalTime (datetime.datetime (today.year, today.month, today.day), step=IntervalTime.HOUR)
    intervals["This month in days"] = IntervalTime (datetime.datetime (today.year, today.month, 1), step=IntervalTime.DAY)
    intervals["This year in months"] = IntervalTime (datetime.datetime (today.year, 1, 1), step=IntervalTime.MONTH)
    intervals["From February to April in days"] = IntervalTime (datetime.datetime (today.year, 2, 1), datetime.datetime (today.year, 4, 1))

    for it in intervals:
        print it
        for i in intervals[it]:
            print "%s - %s" % (str (i.start), str (i.end))
            


        
