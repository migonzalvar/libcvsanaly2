import cairo

class PychaGraph:

    (
        PIE,
        VERTICAL_BAR,
        HORIZONTAL_BAR,
        LINE,
        SCATTER
    ) = range (5)
     

    def __init__ (self, type):
        if type == self.PIE:
            from pycha.pie import PieChart
            self.chart = PieChart
        elif type == self.VERTICAL_BAR:
            from pycha.bar import VerticalBarChart
            self.chart = VerticalBarChart
        elif type == self.HORIZONTAL_BAR:
            from pycha.bar import HorizontalBarChart
            self.chart = HorizontalBarChart
        elif type == self.LINE:
            from pycha.line import LineChart
            self.chart = LineChart
        elif type == self.SCATTER:
            from pycha.scatter import ScatterplotChart
            self.chart = ScatterplotChart
        else:
            # TODO Exception
            pass

    def render (self, filename, width, height, dataset, options = None):
        surface = cairo.ImageSurface (cairo.FORMAT_ARGB32, width, height);

        gfx = self.chart (surface, options)
        gfx.addDataset (dataset)
        gfx.render ()

        surface.write_to_png (filename)
        
if __name__ == '__main__':
    data = (
        ('Graphs',
         (
                ('pie', 160),
                ('vbar', 626),
                ('hbar', 83),
                ('line', 113)
         )
        )
    )

    chart = PychaGraph (PychaGraph.PIE)
    dataset = [(line[0], [[0,line[1]]]) for line in data[1]]
    options = {
        'axis': {
            'x': {
                'ticks': [dict(v=i, label=d[0]) for i, d in enumerate (data[1])],
            }
        },
        'colorScheme': 'blue',
        'background': {
            'baseColor': '#ffffff'
        },
        'legend': {
            'hide': False,
            'position' : {
                'top' : 200,
                'left': 0
            }
         }
    }
    chart.render ("pie.png", 300, 300, dataset, options)

    chart = PychaGraph (PychaGraph.VERTICAL_BAR)
    dataset = ((data[0], [(i, l[1]) for i, l in enumerate (data[1])]),)
    options = {
        'axis': {
            'x': {
                'ticks': [dict(v=i, label=d[0]) for i, d in enumerate (data[1])],
                'label': 'Graph types',
            },
            'y': {
                'tickCount': 4,
                'label': 'Values',
            }
        },
        'background': {
            'chartColor': '#ffeeff',
            'baseColor': '#ffffff',
            'lineColor': '#444444'
        },
        'colorScheme': 'red',
        'padding': {
            'left': 75,
            'bottom': 55,
        },
        'legend' : {
            'hide' : True,
        },
        'title': 'Graphs'
    }
    chart.render ("vbar.png", 400, 600, dataset, options)

    chart = PychaGraph (PychaGraph.HORIZONTAL_BAR)
    chart.render ("hbar.png", 400, 200, dataset, options)

    chart = PychaGraph (PychaGraph.LINE)
    chart.render ("line.png", 400, 200, dataset, options)

    chart = PychaGraph (PychaGraph.SCATTER)
    chart.render ("scatter.png", 400, 400, dataset, options)
